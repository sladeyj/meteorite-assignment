# Meteorite Assignment Team 78
Alex Masterman 
Joe Burnham Slade 
Dylan Scotchmer 
Avinash Kumar 

## File Layout
---

|      File      |               Contains                                  | Parent?  |
| :------------: | :------------------------------------------------------ | -------: |
| Data           | This file contains all of our data files                |  N/A     |
| Original Data  | This is the data that we are working from with no edits |  Data    |
| Working Data   | This is the data that we are activly using              |  Data    |
| Generated Data | This is all the log data that we have generated         |  Data    |
| Graphs         | Contains all of our gernated Graphs                     |  N/A     |
| Report         | Contains all refrence and writing work for the report   |  N/A     |
| Report Files   | Contains all working files for the report               |  Report  |
| Refrences      | Contains all refrences worked from (if applicable)      |  Report  |

---

## Systems in use

For this project we are using:

+ Markdown
+ IBMS' SPSS
+ Git
+ BitBucket
+ Trello

## Links for Project Planning Systems

[BitBucket Page](https://bitbucket.org/sladeyj/meteorite-assignment/src/master/)


[Kanban Board On Trello](https://trello.com/invite/b/fV3RRM5S/8162ce96a49848aa27486b08975ead20/team-research)
